package main

import (
	"fmt"
	"image/png"
	"os"

	"barcode-test/codeGenerator"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/code128"
)

func mmToPixels(mm float64) int {
	return int(mm * 3.7795275591)
}

func main() {
	GTIN := "7701234000011"
	referenceNumber := "9876"
	paymentValue := "25500"
	maxPaymentDate := "20210605"

	code, err := codeGenerator.GenerateCode(GTIN, referenceNumber, paymentValue, maxPaymentDate)

	if err != nil {
		fmt.Println("barcode can't be generated:", err)
		return
	}

	fmt.Println("Creating barcode:", code)

	// Create the barcode
	barCode, err := code128.Encode(code)

	if err != nil {
		fmt.Println("Can't encode barcode:", err)
		return
	}

	width := 50.0

	// Scale the barcode to the desired size
	scaledBarCode, err := barcode.Scale(barCode, mmToPixels(width), mmToPixels(46))

	for err != nil {
		width += 5.0
		scaledBarCode, err = barcode.Scale(barCode, mmToPixels(width), mmToPixels(46))
	}

	fmt.Println("barcode dimensions in mm: ", width, 46)

	// create the output file
	file, err := os.Create("barcode.png")

	if err != nil {
		fmt.Println("Can't saved into file:", err)
		return
	}

	defer file.Close()

	// encode the barcode as png
	png.Encode(file, scaledBarCode)
}
